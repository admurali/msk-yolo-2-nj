var kafka = require('kafka-node');

const client = new kafka.KafkaClient({
    kafkaHost: "b-1.kafka-old-version.oxt09r.c3.kafka.ap-south-1.amazonaws.com:9094,b-2.kafka-old-version.oxt09r.c3.kafka.ap-south-1.amazonaws.com:9094",
    sasl: { mechanism: 'plain', username: 'foo', password: 'bar' }
});

consumer = new kafka.Consumer(
    client,
    [
        { topic: 'SP_PAYMENT_SESSION_RECON_TOPIC'}
    ],
    {
        autoCommit: false
    }
);

consumer.on('message', function (message) {
    console.log(message);
});

console.log('Press any key to continue.');
process.stdin.once('data', function () {
  print('Exit');
});
  